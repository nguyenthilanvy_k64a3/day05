<?php
  session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
        integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="register.css">
    <title>Document</title>
</head>

<body>
    <?php
      $target_dir = "upload";
      if (!is_dir($target_dir)) {
        mkdir("upload");
      } 
      date_default_timezone_set("Asia/Ho_Chi_Minh");
      $target_file = $target_dir . "/" . pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME) . "_" . date("YmdHis") . "." . pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
      if (getimagesize($_FILES["image"]["tmp_name"])) {
        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);
      }
    ?>
    <div class="content mt-3">
        <div class="d-flex align-items-center mt-3">
            <label class="me-5 label">Họ và tên</label>
            <div>
                <?php echo $_POST["name"]; ?>
            </div>
        </div>
        <div class="d-flex align-items-center mt-3">
            <label class="me-5 label">Giới tính</label>
            <div>
                <?php echo $_POST["gender"]; ?>
            </div>
        </div>
        <div class="d-flex align-items-center mt-3">
            <label class="me-5 label">Phân khoa</label>
            <div>
                <?php echo $_POST["khoa"]; ?>
            </div>
        </div>
        <div class="d-flex align-items-center mt-3">
            <label class="me-5 label">Ngày sinh</label>
            <div>
                <?php echo $_POST["ngsinh"]; ?>
            </div>
        </div>
        <div class="d-flex align-items-center mt-3">
            <label class="me-5 label">Địa chỉ</label>
            <div>
                <?php echo $_POST["address"]; ?>
            </div>
        </div>
        <div class="d-flex align-items-start mt-3">
            <label class="me-5 label">Hình ảnh</label>
            <div>
                <?php echo '<img class="image-upload" src="' . $target_file . '" alt="">';?>
            </div>
        </div>
        <input class="btn mt-3 text-light" type="submit" value="Xác nhận" onClick="document.location.href='search.php'">
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"
        integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

</body>

</html>