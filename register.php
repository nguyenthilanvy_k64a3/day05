<?php
    session_start();
        // define variables and set to empty values
        $nameErr = $genderErr = $khoaErr = $ngsinhErr = $imgErr = "";
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
          if (empty($_POST["name"])) {
            $nameErr = "Hãy nhập tên.";
            header("Location: register.php");
          }
          if (empty($_POST["gender"])) {
            $genderErr = "Hãy chọn giới tính.";
            header("Location: register.php");
          }
          if (empty($_POST["khoa"])) {
            $khoaErr = "Hãy chọn phân khoa.";
            header("Location: register.php");
          }
          if (empty($_POST["ngsinh"])) {
            $ngsinhErr = "Hãy nhập ngày sinh.";
            header("Location: register.php");
          }
        }
      ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
        integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="register.css">
    <title>Document</title>
</head>

<body>

    <div class="content mt-3">
        <span class="error"><?php if ($nameErr) {echo $nameErr . '<br>';} ?></span>
        <span class="error"><?php if ($genderErr) {echo $genderErr . '<br>';} ?></span>
        <span class="error"><?php if ($khoaErr) {echo $khoaErr . '<br>';} ?></span>
        <span class="error"><?php if ($ngsinhErr) {echo $ngsinhErr . '<br>';} ?></span>
        <span class="error"><?php if ($imgErr) {echo $imgErr . '<br>';} ?></span>

        <form method="post" action="confirm.php" enctype="multipart/form-data">
            <div class="d-flex mt-3">
                <label class="me-3 label">Họ và tên <span class="required-input">*</span></label>
                <input class="form-control input" type="text" name="name">
            </div>
            <div class="d-flex align-items-center mt-3">
                <label class="me-3 label">Giới tính <span class="required-input">*</span></label>
                <?php
                  $gt = array("0"=>"Nam", "1"=>"Nữ");
                  for ($i = 0; $i<count($gt); $i++) {
                      echo '<div class="form-check form-check-inline">
                      <input class="form-check-input input" type="radio" name="gender" id="gender'. $i .'" value="' . $gt[$i] . '">
                      <label class="form-check-label text-dark" for="gender'. $i .'">'. $gt[$i] .'</label>
                      </div>';
                  }
              ?>
            </div>
            <div class="d-flex mt-3">
                <label class="me-3 label">Phân khoa <span class="required-input">*</span></label>
                <select name="khoa" id="" class="form-select input">
                    <option value=""></option>
                    <?php
                      $khoa = array("MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");
                      foreach ($khoa as $key => $value) {
                          echo '<option value="'.$value.'">'.$value.'</option>';
                      }

                  ?>
                </select>
            </div>
            <div class="d-flex mt-3">
                <label class="me-3 label">Ngày sinh <span class="required-input">*</span></label>
                <input class="form-control input datepicker" type="text" placeholder="dd/mm/yyyy" name="ngsinh">
            </div>
            <div class="d-flex mt-3">
                <label class="me-3 label">Địa chỉ</label>
                <input class="form-control input" type="text" name="address">
            </div>
            <div class="d-flex mt-3">
                <label class="me-3 label">Hình ảnh</label>
                <input class="form-control input" type="file" name="image">
            </div>
            <input class="btn mt-3 text-light" type="submit" value="Đăng ký">
        </form>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"
        integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
        integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy'
    })
    </script>
</body>

</html>