<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
        integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="register.css">
    <title>Document</title>
</head>

<body>
    <div class="content mt-3">
        <div class="w-75 search-bar">
            <div class="d-flex mt-3">
                <div class="me-3 w-25">Khoa</div>
                <select name="khoa" class="form-select input">
                    <option value=""></option>
                    <?php
                      $khoa = array("MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");
                      foreach ($khoa as $key => $value) {
                          echo '<option value="'.$value.'">'.$value.'</option>';
                      }

                  ?>
                </select>
            </div>
            <div class="d-flex mt-3">
                <div class="me-3 w-25">Từ khóa</div>
                <input class="form-control input" type="text" name="keyword">
            </div>
            <input class="btn mt-3 mb-3 text-light" type="submit" value="Tìm kiếm">
        </div>

        <div class="d-flex mt-3 pb-3 justify-content-between align-items-center">
            <div>Số sinh viên tìm thấy: XXX</div>
            <input class="btn text-light" type="submit" value="Thêm" onClick="document.location.href='register.php'">
        </div>
        <div class="row text-start mt-3">
            <div class="col col-1">
                No
            </div>
            <div class="col col-4">
                Tên sinh viên
            </div>
            <div class="col col-5">
                Khoa
            </div>
            <div class="col col-2">
                Action
            </div>
        </div>
        <hr class="m-0">
        <div class="row mt-1 text-start">
            <div class="col col-1">
                1
            </div>
            <div class="col col-4">
                Nguyễn Văn A
            </div>
            <div class="col col-5">
                Khoa học máy tính
            </div>
            <div class="col col-2">
                <div class="d-flex align-items-center justify-content-between">
                    <input class="btn btn-sm text-light" type="submit" value="Xóa">
                    <input class="btn btn-sm text-light" type="submit" value="Sửa">
                </div>
            </div>
        </div>
        <div class="row mt-1 text-start">
            <div class="col col-1">
                2
            </div>
            <div class="col col-4">
                Nguyễn Văn B
            </div>
            <div class="col col-5">
                Khoa học máy tính
            </div>
            <div class="col col-2">
                <div class="d-flex align-items-center justify-content-between">
                    <input class="btn btn-sm text-light" type="submit" value="Xóa">
                    <input class="btn btn-sm text-light" type="submit" value="Sửa">
                </div>
            </div>
        </div>
        <div class="row mt-1 text-start">
            <div class="col col-1">
                3
            </div>
            <div class="col col-4">
                Hoàng Văn C
            </div>
            <div class="col col-5">
                Khoa học vật liệu
            </div>
            <div class="col col-2">
                <div class="d-flex align-items-center justify-content-between">
                    <input class="btn btn-sm text-light" type="submit" value="Xóa">
                    <input class="btn btn-sm text-light" type="submit" value="Sửa">
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"
        integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</body>

</html>